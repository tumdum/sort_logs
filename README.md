# README #

This small tool sorts log lines saved from multiple sources sharing same time source. In case of fomrat that this tool handles there are two important columns: time and counter.

Time contains information when given log was *generated* by application. Counter is one byte and is incremented each time log is *saved*. sort_logs orders all log lines by time and for entries with same time orders by counter.