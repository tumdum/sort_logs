extern crate logparse;
extern crate toml;
extern crate regex;
#[macro_use]
extern crate log;
extern crate env_logger;

use std::io::{BufRead,Read};
use std::vec::Vec;
use std::fs::File;

fn parse_config() -> Option<logparse::Config> {
    let mut path = match std::env::home_dir() {
        None => {
            warn!("Home dir not set");
            return None
        },
        Some(path) => path
    };
    path.push(".sort_logs");
    let mut f = match File::open(&path) {
        Err(e) => {
            warn!("Failed to open {:?}: {}", &path, e);
            return None
        }
        Ok(f)  => f
    };
    let mut buf = String::new();
    if let Err(e) = f.read_to_string(&mut buf) {
        warn!("Failed to read {:?}: {}", &path, e);
        return None;
    }
    let mut parser = toml::Parser::new(&buf);
    let toml = match parser.parse() {
        None    => {
            warn!("Failed to parse {:?}", &path);
            for error in parser.errors {
                warn!("\terror: {:?}", error);
            }
            return None
        },
        Some(t) => t
    };
    let entries = match toml.get("entry") {
        Some(&toml::Value::Array(ref entries)) => entries,
        None | Some(_) => {
            warn!("Expected array of 'entry'.");
            return None
        },
    };
    let mut config = logparse::Config(vec![]);
    for entry in entries {
        if let &toml::Value::Table(ref table) = entry {
            if let Some(t) = parse_entry(&table) {
                config.0.push(t);
            }
        }
    }
    Some(config)
}

fn parse_entry(entry: &toml::Table) -> Option<(regex::Regex, Vec<String>)> {
    let regex = match entry.get("regex") {
        Some(&toml::Value::String(ref s)) => match regex::Regex::new(&s) {
            Err(e) => {
                warn!("Failed to parse {}: {:?}", s, e);
                return None;
            },
            Ok(re) => re
        },
        None | Some(_) => return None,
    };
    let times_strings = match entry.get("times") {
        Some(&toml::Value::Array(ref strings)) => {
            let mut v : Vec<String> = Vec::new();
            for string in strings {
                if let &toml::Value::String(ref s) = string {
                    v.push(s.clone());
                }
            }
            v
        },
        None | Some(_) => return None
    };
    Some((regex, times_strings))
}

fn main() {
    env_logger::init().unwrap();

    let parser = match parse_config() {
        Some(c) => logparse::Parser::new(c),
        None    => logparse::Parser::default()
    };
    let reader = std::io::BufReader::new(std::io::stdin());
    let logs = reader.lines()
        .filter_map(|l| l.ok().map(|ll| parser.parse(&ll)));
    let mut sorted : Vec<logparse::Log> = vec![];
    for log in logs {
        match log {
            Ok(l) => sorted.push(l),
            Err(e) => { warn!("Failed to parse line: {}", e); () }
        }
    }
    sorted.sort();
    for log in sorted {
        println!("{}", log.line);
    }
}
