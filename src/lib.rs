extern crate time;
extern crate regex;

use std::result;
use std::cmp::Ordering;

#[derive(Debug, Clone)]
pub struct Log {
    time: time::Tm,
    counter: u8,
    pub line: String,
}

impl PartialEq for Log {
    fn eq(&self, other: &Log) -> bool {
        self.time == other.time && self.counter == other.counter
    }
}

impl Eq for Log {}

impl PartialOrd for Log {
    fn partial_cmp(&self, other: &Log) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for Log {
    fn cmp(&self, other: &Log) -> Ordering {
        let time_ord = self.time.cmp(&other.time);
        if time_ord == Ordering::Equal {
            return self.counter.cmp(&other.counter);
        }
        time_ord
    }
}

pub struct Config(pub Vec<(regex::Regex, Vec<String>)>);

pub struct Parser {
    config: Config
}

impl Parser {
    pub fn default() -> Parser {
        let main_regex = regex::Regex::new(r"\b(?P<counter>.{2}) \S+ <(?P<time>[^>]+)> ").unwrap();
        let time_strings = vec!["%m.%d %H:%M:%S.%f".to_string(),"%Y-%m-%dT%H:%M:%S.%f".to_string()];
        Parser{config: Config(vec![(main_regex, time_strings)])}
    }

    pub fn new(config: Config) -> Parser {
        Parser{config: config}
    }

    pub fn parse(&self, input: &str) -> result::Result<Log, String> {
        match self.parse_line(input) {
            Some(l) => return Ok(l),
            None    => return Err(input.to_string())
        }
    }

    fn parse_line(&self, input: &str) -> Option<Log> {
        for pair in &self.config.0 {
            let cap = match pair.0.captures(&input) {
                None    => continue,
                Some(c) => c,
            };
            let counter = match cap.name("counter") {
                None    => continue,
                Some(m) => match u8::from_str_radix(m, 16) {
                    Err(_) => continue,
                    Ok(c)  => c,
                }
            };
            let time = match self.parse_time(&pair.1, cap.name("time")) {
                None    => continue,
                Some(t) => t
            };
            return Some(Log{
                time: time,
                counter: counter,
                line: input.to_string()})
        }
        None
    }

    fn parse_time(&self, time_strings: &Vec<String>, capture: Option<&str>) -> Option<time::Tm> {
        let m = match capture {
            None    => return None,
            Some(m) => m
        };
        for s in time_strings {
            if let Ok(mut t) = time::strptime(&m, s) {
                if t.tm_year != 0 {
                    t.tm_year += 1900
                }
                return Some(t)
            }
        }
        None
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use time;

    fn expected_time() -> time::Tm {
        time::Tm{
            tm_sec: 36, tm_min: 35, tm_hour: 13,
            tm_mday: 19, tm_mon: 9, tm_year: 0,
            tm_wday: 0, tm_yday: 0, tm_isdst: 0,
            tm_utcoff: 0, tm_nsec: 981305000}
    }

    #[test]
    fn parser_parse_should_handle_first_format() {
        let input = "000001 10.19 13:35:17.416 [127.0.0.1] 12 BOARD-1234-3-AppName <10.19 13:35:36.981305> 270 DBG/ABC/XYZ, something something".to_string();
        let log = Parser::default().parse(&input).unwrap();
        let expected_log = Log{
            time: expected_time(),
            counter: 0x12,
            line: input.clone()};
        assert_eq!(expected_log, log);
    }

    #[test]
    fn parser_parse_should_handle_second_format() {
        let input = "12 BOARD-1234-3-AppName <10.19 13:35:36.981305> 270 INF/ABC/XYZ, something something something".to_string();
        let log = Parser::default().parse(&input).unwrap();
        let expected_log = Log{
            time: expected_time(),
            counter: 0x12,
            line: input.clone()};
        assert_eq!(expected_log, log);
    }

    #[test]
    fn parser_parse_should_handle_third_format() {
        let input = "123456 02.11 17:20:30.92  [192.168.1.1]  57 BOARD-1234-3-AppName <2015-10-19T13:35:36.981305Z> 270-ThreadName DBG/ABC/File.cpp#42 something something something".to_string();
        let log = Parser::default().parse(&input).unwrap();
        let mut expected_time = expected_time();
        expected_time.tm_year = 2015;
        let expected_log = Log{
            time: expected_time,
            counter: 0x57,
            line: input.clone()};
        assert_eq!(expected_log, log);
    }

    #[test]
    fn log_relations() {
        let mut smaller_time = expected_time();
        smaller_time.tm_mon -= 1;
        let smaller_time = smaller_time;
        let expected_log = Log{time: expected_time(), counter: 0x10, line: "".to_string()};
        let mut expected_eq_log = expected_log.clone();
        expected_eq_log.line = "test".to_string();
        let expected_eq_log = expected_eq_log;
        let smaller_time_log = Log{time: smaller_time, counter: 0x10, line: "".to_string()};
        let smaller_counter_log = Log{time: expected_time(), counter: 0x9, line: "".to_string()};

        assert_eq!(expected_log, expected_log);
        assert_eq!(expected_log, expected_eq_log);
        assert!(expected_log != smaller_time_log);
        assert!(expected_log != smaller_counter_log);

        assert!(expected_log > smaller_time_log);
        assert!(expected_log > smaller_counter_log);

        assert!(!(expected_log > expected_eq_log));
        assert!(!(expected_log < expected_eq_log));
    }
}
